@extends('layouts.app')

@section('konten')
<div class="container mt-5">
  <div class="d-flex justify-content-between">
      <h3 class="mr-auto">Invoice List</h3>
      {{-- <button type="button" class="btn btn-success" id="add-invoice">Add Invoice</button> --}}
      <button type="button" class="btn btn-success" id="add-invoice">
        Add Invoice
      </button>
  </div>
  <div class="row justify-content-center">
    <div class="col-md mt-2">
      <table id="invoice-lists" class="display table table-striped" style="width:100%">
      </table>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalInvoice" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
      </div>
      <div class="modal-body">
        {!! Form::open(['id' => 'form-invoice']) !!}
          <div class="form-group">
            <label for="from-customer">From</label>
            <select class="form-control" id="from-customer"></select>
          </div>
          <div class="form-group">
            <label for="to-customer">To</label>
            <select class="form-control" id="to-customer"></select>
          </div>
          <div class="form-group">
            <label for="subject">Subject</label>
            <input type="text" class="form-control" id="subject">
          </div>
          <div class="form-group">
            <label for="issue-date">Issue Date</label>
            <input type="date" class="form-control" id="issue-date">
          </div>
          <div class="form-group">
            <label for="due-date">Due Date</label>
            <input type="date" class="form-control" id="due-date">
          </div>
          <div class="d-flex justify-content-between mt-3">
            <h4 class="mr-auto">Products</h4>
            <button type="button" class="btn btn-primary" id="add-products">
              Add Product
            </button>
          </div>
          <table id="products" class="display table table-striped" style="width:100%">
            <thead>
              <tr>
                <th>Product Name</th>
                <th>Category</th>
                <th>Quantity</th>
                <th>Unit Price</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody id="product-list-selected"></tbody>
          </table>
          <div class="form-group">
            <label for="subtotal">Subtotal</label>
            <input type="text" class="form-control" id="subtotal" disabled>
          </div>
          <div class="form-group">
            <label for="tax">Tax (10%)</label>
            <input type="text" class="form-control" id="tax" disabled>
          </div>
          <div class="form-group">
            <label for="payment">Payment</label>
            <input type="text" class="form-control" id="payment" disabled>
          </div>
          <button type="submit" id="on-submit" class="btn btn-primary mt-2">Submit</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>

<!-- Modal Products -->
<div class="modal fade" id="modalProducts" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Product</h5>
      </div>
      <div class="modal-body">
        {!! Form::open(['id' => 'form-product']) !!}
        <div class="form-group">
          <label for="product-name">Product Name</label>
          <select class="form-control" id="product-name"></select>
        </div>
        <div class="form-group">
          <label for="amount">Amount</label>
          <input type="text" class="form-control" id="amount-product" disabled>
        </div>
        <div class="form-group">
          <label for="quantity">Quantity</label>
          <input type="number" class="form-control" id="quantity" min="0">
          <input type="hidden" class="form-control" id="product">
          <input type="hidden" class="form-control" id="item-type">
          <input type="hidden" class="form-control" id="category">
        </div>
        <button type="button" id="btn-add-product" class="btn btn-success mt-2">Add</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection


<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
<script>
  let isNew = true,
    productSelected = []

  $(document).ready(function() {
    let title = isNew ? "Add Invoice" : "Edit Invoice"
    $('#modalInvoice .modal-title').text(title)
    initDataTable()

    if($(`#from-customer option:selected`).val() == undefined) $('#to-customer').prop('disabled', true)

    $('#add-invoice').on('click', function () {
      modalAdd()
    })

    $('#from-customer').on('change', function () {
      if($(`#from-customer option:selected`).val() == '') {
        $('#to-customer').empty()
        $('#to-customer').prop('disabled', true)
        return
      } else {
        $('#to-customer').prop('disabled', false)
      }
      getCustomer(true, $(`#from-customer option:selected`).val(), true)
    })

    $('#product-name').on('change', function () {
      getProductDetail($(`#product-name option:selected`).val())
    })

    $('#btn-add-product').on('click', async function () {
      $('#btn-add-product').prop('disabled', true)
      await onStoreProducts()
      document.getElementById("form-product").reset()
      $('#btn-add-product').prop('disabled', false)
      $('#modalProducts').modal('hide')
      onLoadProductSelected()
    })

    $('#form-invoice').submit(function (event) {
      event.preventDefault()
      onSubmit()
    })
  });


  function initDataTable() {
    $('#invoice-lists').DataTable({
      processing: true,
      serverSide: true,
      searchDelay: 1000,
      ajax: {
        url: '{{ route('invoice.search') }}',
        type: 'GET',
        data: {
          is_dataTable: true
        }
      },
      columns: [
        {
          title: 'No',
          data: null,
          sortable: false,
          searchable: false,
          render: function (data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
          }
        },
        {
          data: 'subject',
          title: 'Subject'
        },
        {
          data: 'issue_date',
          title: 'Issue Date'
        },
        {
          data: 'due_date',
          title: 'Due Date'
        },
        {
          title: 'Paid',
          data: null,
          searchable: false,
          render: function (data, type, row, meta) {
            if (data.is_paid) return 'Paid'
            return 'Unpaid'
          }
        },
        {
          data: 'total_amount',
          title: 'Total Amount'
        },
        {
          title: 'Action',
          data: null,
          sortable: false,
          searchable: false,
          render: function (data, type, row, meta) {
            return `
              <button type="button" class="btn btn-primary" onclick="showInvoice('${data.slug_path}')">Detail</button>
              <button type="button" class="btn btn-warning">Update</button>
              <button type="button" class="btn btn-danger" onclick="deleteInvoice('${data.slug_path}')">Delete</button>
            `
          }
        },
      ]
    });
  }

  function deleteInvoice(slug_path) {
    $.ajax({
      url: '{{ route('invoice.remove') }}',
      type: 'DELETE',
      data: {
        slug_path: slug_path,
      },
      success: function () {
        // initDataTable()
        location.reload()
      },
      error: function () {
        alert('Something went wrong!');
      }
    });
  }

  function showInvoice(slug_path) {
    let slug = slug_path
    let url = "{{ route('detail-invoice', ':slug_path') }}";
    url = url.replace(':slug_path', slug);
    location.href = url;
  }

  async function getCustomer(isNew = true, selected = null, isToCustomer = false) {
    let payload = (isNew && isToCustomer == false) ? null : {
      selected: selected
    }

    $.ajax({
      url: '{{ route('customer.search') }}',
      type: 'GET',
      data: payload,
      success: function (response) {
        let res = response.data
        var html = '<option value=""> - </option>'
        for (idx = 0; idx < res.length; idx++) {
          html = html + `
            <option value=${res[idx].slug_path}> ${res[idx].name} </option>
          `
        }
        if (isToCustomer) return $('#to-customer').html(html)
        $('#from-customer').html(html)
      },
      error: function () {
        alert('Something went wrong!');
      }
    });
  }

  async function modalAdd() {
    let customers = await getCustomer()
    $('#modalInvoice').modal('show')

    $('#add-products').on('click', function () {
      openModalProduct()
    })
  }

  async function openModalProduct() {
    let products = await getProducts()
    $('#modalProducts').modal('show')
  }

  async function getProducts(currentSelect = null) {
    let payload = currentSelect ? {
      selected: selected,
    } : null

    $.ajax({
      url: '{{ route('product.search') }}',
      type: 'GET',
      data: payload,
      success: function (response) {
        let res = response.data
        var html = '<option value=""> - </option>'
        for (idx = 0; idx < res.length; idx++) {
          html = html + `
            <option value=${res[idx].slug_path}> ${res[idx].name} </option>
          `
        }
        $('#product-name').html(html)
      },
      error: function () {
        alert('Something went wrong!');
      }
    });
  }

  async function getProductDetail(slug_path) {
    $.ajax({
      url: '{{ route('product.detail') }}',
      type: 'GET',
      data: {
        slug_path: slug_path,
      },
      success: function (response) {
        let res = response.data
        $('#amount-product').val(res.amount)
        $('#product').val(res.name)
        $('#item-type').val(res.item_type_id)
        $('#category').val(res.category)
      },
      error: function () {
        alert('Something went wrong!');
      }
    });
  }

  function onStoreProducts() {
    let slugProduct = $(`#product-name option:selected`).val(),
      nameProduct = $('#product').val(),
      amountProduct = $('#amount-product').val(),
      itemType = $('#item-type').val(),
      category = $('#category').val(),
      qty = $('#quantity').val()

    if (slugProduct == '') {
      alert('select product')
      return
    }
    if (qty == '') {
      alert('qty still empty')
      return
    }

    let data = {
      slugProduct: slugProduct,
      nameProduct: nameProduct,
      unitPrice: amountProduct,
      amountProduct: parseInt(amountProduct) * parseInt(qty),
      itemType: itemType,
      category: category,
      qty: qty,
    }

    productSelected.push(data)
  }

  function onLoadProductSelected() {
    var html = ''
    for (let index = 0; index < productSelected.length; index++) {
      html = html + `
        <tr>
          <td>${productSelected[index].nameProduct}</td>
          <td>${productSelected[index].category}</td>
          <td>${productSelected[index].qty}</td>
          <td>${productSelected[index].unitPrice}</td>
          <td>${productSelected[index].amountProduct}</td>
        </tr>
      `
    }
    $('#product-list-selected').html(html)

    let subtotal = productSelected.reduce((n, {amountProduct}) => n + amountProduct, 0)
    let tax = subtotal * (10 / 100)
    let totalPayment = subtotal + tax

    $('#subtotal').val(subtotal)
    $('#tax').val(tax)
    $('#payment').val(totalPayment)
  }

  function onSubmit() {
    let fromCustomer = $('#from-customer option:selected').val(),
      toCustomer = $('#to-customer option:selected').val(),
      subject = $('#subject').val(),
      issueDate = $('#issue-date').val(),
      dueDate = $('#due-date').val(),
      subtotal = $('#subtotal').val(),
      tax = $('#tax').val(),
      payment = $('#payment').val()

    $.ajax({
      url: '{{ route('invoice.store') }}',
      type: 'POST',
      data: {
        fromCustomer: fromCustomer,
        toCustomer: toCustomer,
        subject: subject,
        issueDate: issueDate,
        dueDate: dueDate,
        subtotal: subtotal,
        tax: tax,
        payment: payment,
        products: productSelected,
      },
      success: function () {
        document.getElementById("form-invoice").reset()
        productSelected = []
        // initDataTable()
        $('#modalInvoice').modal('hide')
        location.reload()
      },
      error: function () {
        alert('Something went wrong!');
      }
    });
  }

</script>
