## Tes programming ESB

langkah menjalankan tes:

- git clone.
- composer install.
- php artisan migrate.
- php artisan db:seed.

## NOTES
- halaman edit belum sama sekali.
- halaman detail baru hit api dan belum tampil di front end.
- halaman tambah dan hapus sudah selesai


## untuk menjalankan api list invoice :
## LIST INVOICES
- http://localhost/api/invoice/search

## DETAIL INVOICE
- http://localhost/api/invoice/show?slug_path={{ by slug }}
