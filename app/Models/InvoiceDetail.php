<?php

namespace App\Models;

use Balping\HashSlug\HasHashSlug;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    use HasFactory, HasHashSlug;

    protected $guarded = [];
    protected $table = 'invoice_detail';
    protected $appends = [
        'product_detail'
    ];

    public function productInvoice()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function getProductDetailAttribute()
    {
        $data = $this->productInvoice()->first();
        if ($data) return $data;
        return null;
    }
}
