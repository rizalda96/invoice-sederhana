<?php

namespace App\Models;

use Balping\HashSlug\HasHashSlug;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory, HasHashSlug;

    protected $guarded = [];
    protected $table = 'customer_info';

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function province()
    {
        return $this->hasOne(Province::class, 'id', 'province_id');
    }
    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
}
