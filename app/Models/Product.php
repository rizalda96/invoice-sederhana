<?php

namespace App\Models;

use Balping\HashSlug\HasHashSlug;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, HasHashSlug;

    protected $table = "product";
    protected $guard = [];

    public function itemType()
    {
        return $this->hasOne(ItemType::class, 'id', 'item_type_id');
    }
}
