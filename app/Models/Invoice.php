<?php

namespace App\Models;

// use App\Models\Traits\HasHashSlug;
use Balping\HashSlug\HasHashSlug;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory, HasHashSlug;

    protected $guarded = [];
    protected $table = 'invoice';
    protected $casts = [
        'is_paid' => 'boolean',
    ];
    protected $appends = [
        'invoice_detail',
        'from_customer',
        'to_customer'
    ];

    public function invoiceDetails()
    {
        return $this->hasMany(InvoiceDetail::class, 'invoice_id', 'id');
    }

    public function from_customer()
    {
        return $this->hasOne(Customer::class, 'id', 'from_id');
    }

    public function to_customer()
    {
        return $this->hasOne(Customer::class, 'id', 'to_id');
    }

    public function getInvoiceDetailAttribute()
    {
        $data = $this->invoiceDetails()->get();
        if ($data) return $data;
        return [];
    }

    public function getFromCustomerAttribute()
    {
        $data = $this->from_customer()->first();
        if ($data) return $data;
        return null;
    }

    public function getToCustomerAttribute()
    {
        $data = $this->to_customer()->first();
        if ($data) return $data;
        return null;
    }
}
