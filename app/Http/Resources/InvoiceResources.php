<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResources extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    $data = [
      'subject' => $this->subject,
      'issue_date' => $this->issue_date,
      'due_date' => $this->due_date,
      'tax_id' => $this->tax_id,
      'total_amount' => $this->total_amount,
      'is_paid' => $this->is_paid,
      'slug_path' => $this->slug_path,
    ];


    return $data;
  }
}
