<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
  protected $product;

  public function __construct(Product $product)
  {
    $this->product = $product;
  }

  public function search(Request $request)
  {
    $query = $this->product->query()
      ->with('itemType');

    if ($request->selected) {
      $customerFrom = $this->product->findBySlug($request->selected);
      $query = $query->where('id', '!=', $customerFrom->id);
    }

    $query = $query->get();

    if ($query) {
      return jsend_success($query, 'Data ditemukan!');
    } else {
      return jsend_fail(null, 'Data tidak ditemukan');
    }
  }

  public function detail(Request $request)
  {
    $query = $this->product->findBySlug($request->slug_path);

    if ($query) {
      return jsend_success($query, 'Data ditemukan!');
    } else {
      return jsend_fail(null, 'Data tidak ditemukan');
    }
  }
}
