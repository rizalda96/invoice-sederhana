<?php

namespace App\Http\Controllers;

use App\Http\Resources\InvoiceResources;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{

  protected $invoice;
  protected $invoiceDetail;
  protected $customer;
  protected $product;

  public function __construct(
    Invoice $invoice,
    InvoiceDetail $invoiceDetail,
    Product $product,
    Customer $customer
  ) {
      $this->invoice = $invoice;
      $this->invoiceDetail = $invoiceDetail;
      $this->product = $product;
      $this->customer = $customer;
  }

  //
  public function index()
  {
    return view('modules.invoice.index');
  }

  public function detail()
  {
    return view('modules.invoice.detail');
  }

  public function search(Request $request)
  {
    $search = collect($request->search);
    $search = $search->shift();

    $query = $this->invoice->query();

    // handling searchBy
    if ($search) {
      $query->where(function ($sql) use ($request, $search) {
        $idx = 0;
        $filteredColumns = collect($request->columns)->filter(function ($value) {
          return $value['searchable'] === "true";
        });

        foreach ($filteredColumns as $column) {
          $clause = $idx == 0 ? 'where' : 'orWhere';
          $sql->{$clause}(\DB::raw("{$column['data']}"), 'like', '%' . $search . '%');
          ++$idx;
        }
      });
    }

    $query = $query->get();

    $res = InvoiceResources::collection($query);

    if ($request->is_dataTable) {
      return [
        'draw' => $request->draw,
        'recordsTotal' => count($query),
        'recordsFiltered' => count($query),
        'data' => $res,
      ];
    }

    if ($query) {
      return jsend_success($res, 'Data ditemukan!');
    } else {
      return jsend_fail(null, 'Data tidak ditemukan');
    }
  }

  public function remove(Request $request)
  {
    $data = $this->invoice->findBySlug($request->slug_path);
    // $data->invoiceDetails()->delete();
    if (!$data) return jsend_fail(null, 'Data tidak ditemukan');

    InvoiceDetail::where('invoice_id', $data->id)->delete();
    $data->delete();
    return jsend_success(null, 'Data berhasil dihapus!');
  }

  public function store(Request $request)
  {
    DB::beginTransaction();
    try {
      $fromCustomer = $this->customer->findBySlug($request->fromCustomer);
      $toCustomer = $this->customer->findBySlug($request->toCustomer);

      $data = [
        'from_id' => $fromCustomer->id,
        'to_id' => $toCustomer->id,
        'subject' => $request->subject,
        'issue_date' => $request->issueDate,
        'due_date' => $request->dueDate,
        'subtotal' => $request->subtotal,
        'tax' => $request->tax,
        'total_amount' => $request->payment,
        // 'total_amount_due' => 0?,
        'is_paid' => false,
      ];

      $invoice = $this->invoice->create($data);

      $products = collect($request->products);

      $products->map(function ($row, $key) use ($invoice, $request) {
        $product = $this->product->findBySlugOrFail($row['slugProduct']);
        $this->invoiceDetail->create([
          'invoice_id' => $invoice->id,
          'product_id' => $product->id,
          'qty' => $row['qty'],
          'unit_price' => $row['unitPrice'],
          'amount' => $row['amountProduct']
        ]);
      });


      DB::commit();
      return jsend_success(null, 'Data berhasil dihapus!');
    } catch (\Throwable $th) {
      //throw $th;
      $errorMsg       = $th->getMessage();
      DB::rollBack();
      return jsend_fail(null, 'Data Gagal Disimpan.' . $errorMsg);
    }
  }

  public function show(Request $request)
  {
    $query = $this->invoice->findBySlug($request->slug_path);

    if ($query) {
      return jsend_success($query, 'Data ditemukan!');
    } else {
      return jsend_fail(null, 'Data tidak ditemukan');
    }
  }
}
