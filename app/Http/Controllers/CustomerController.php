<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{

  protected $customer;

  public function __construct(Customer $customer)
  {
    $this->customer = $customer;
  }

  public function search(Request $request)
  {
    $query = $this->customer->query()
      ->with('country')
      ->with('province')
      ->with('city');

    if ($request->selected) {
      $customerFrom = $this->customer->findBySlug($request->selected);
      $query = $query->where('id', '!=', $customerFrom->id);
    }

    $query = $query->get();

    // $res = CustomerResources::collection($query);

    if ($query) {
      return jsend_success($query, 'Data ditemukan!');
    } else {
      return jsend_fail(null, 'Data tidak ditemukan');
    }
  }
}
