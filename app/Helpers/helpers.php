<?php

use Carbon\Carbon;
use App\Models\AuditTrails;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

if (! function_exists('encrypt_hashid')) {
  /**
   * Return the route to the "home" page depending on authentication/authorization status.
   *
   * @return string
   */
  function encrypt_hashid($id)
  {
		$length = config('hashslug.minSlugLength', 5);

    $alphabet = config('hashslug.alphabet', 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
    $salt = config('hashslug.appsalt', config('app.key'));
    $salt = hash('sha256', $salt);

    $hashIds = new \Hashids\Hashids($salt, $length, $alphabet);
    $temp = bin2hex($id);
    return $hashIds->encodeHex($temp);
  }
}

if (! function_exists('decrypt_hashid')) {
  /**
   * Return the route to the "home" page depending on authentication/authorization status.
   *
   * @return string
   */
  function decrypt_hashid($slug)
  {
		$length = config('hashslug.minSlugLength', 5);

    $alphabet = config('hashslug.alphabet', 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
    $salt = config('hashslug.appsalt', config('app.key'));
    $salt = hash('sha256', $salt);

    $hashIds = new \Hashids\Hashids($salt, $length, $alphabet);
		$decoded = $hashIds->decodeHex($slug);


		if(! isset($decoded)){
			return null;
		}

		return hex2bin($decoded);
  }
}

if (!function_exists("jsend_error")) {
  /**
   * @param string $message Error message
   * @param string $code Optional custom error code
   * @param string | array $data Optional data
   * @param int $status HTTP status code
   * @param array $extraHeaders
   * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
   */
  function jsend_error($message, $code = null, $data = null, $status = 500, $extraHeaders = [])
  {
      $response = [
          "status" => "error",
          "message" => $message
      ];
      !is_null($code) && $response['code'] = $code;
      !is_null($data) && $response['data'] = $data;
      return response()->json($response, $status, $extraHeaders);
  }
}

if (!function_exists("jsend_fail")) {
  /**
   * @param array $data
   * @param int $status HTTP status code
   * @param array $extraHeaders
   * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
   */
  function jsend_fail($data, $message=null, $status = 400, $extraHeaders = [])
  {
      $response = [
          "status" => "fail",
          "data" => $data
      ];
      !is_null($message) && $response['message'] = $message;

      return response()->json($response, $status, $extraHeaders);
  }
}

if (!function_exists("jsend_success")) {
  /**
   * @param array | Illuminate\Database\Eloquent\Model $data
   * @param int $status HTTP status code
   * @param array $extraHeaders
   * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
   */
  function jsend_success($data, $message=null, $status = 200, $extraHeaders = [])
  {
      $response = [
          "status" => "success",
          "data" => $data
      ];
      !is_null($message) && $response['message'] = $message;
      return response()->json($response, $status, $extraHeaders);
  }
}

if (!function_exists('encrypt_params')) {
  function encrypt_params($id)
  {
    $length = 5;

    $salt = base64_encode(config('app.name'));

    $hashIds = new \Hashids\Hashids($salt, $length);
    $temp = bin2hex($id);
    return $hashIds->encodeHex($temp);
  }
}

if (!function_exists('decrypt_params')) {
  function decrypt_params($id)
  {
    $length = 5;

    $salt = base64_encode(config('app.name'));

    $hashIds = new \Hashids\Hashids($salt, $length);
    $decoded = $hashIds->decodeHex($id);

    if (!isset($decoded)) {
      return null;
    }
    return hex2bin($decoded);
  }
}

