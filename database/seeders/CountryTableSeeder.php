<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =
            [
                'code' => 'INA',
                'name' => 'INDONESIA',
                'created_at' => now(),
                'updated_at' => now(),
            ];

        Country::create($data);
    }
}
