<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'item_type_id' => 1,
                'name' => "Design",
                'category' => "Service",
                'desc' => "Design",
                'amount' => 100000,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'item_type_id' => 1,
                'name' => "Development",
                'category' => "Service",
                'desc' => "Development",
                'amount' => 200000,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'item_type_id' => 1,
                'name' => "Meetings",
                'category' => "Service",
                'desc' => "Meetings",
                'amount' => 300000,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];

        foreach ($data as $value) {
            Product::create($value);
        }
    }
}
