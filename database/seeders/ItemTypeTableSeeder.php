<?php

namespace Database\Seeders;

use App\Models\ItemType;
use Illuminate\Database\Seeder;

class ItemTypeTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $data = [
      [
        'type_name' => 'service',
        'created_at' => now(),
        'updated_at' => now(),
      ],
      [
        'type_name' => 'product',
        'created_at' => now(),
        'updated_at' => now(),
      ]
    ];

    foreach ($data as $value) {
      ItemType::create($value);
    }
  }
}
