<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Seeder;

class CustomerInfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Siti',
                'country_id' => 1,
                'province_id' => 15,
                'city_id' => 30,
                'address' => 'taman safari',
                'zip_code' => 14045,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'zaenab',
                'country_id' => 1,
                'province_id' => 17,
                'city_id' => 23,
                'address' => 'cisarua',
                'zip_code' => 14046,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];

        foreach ($data as $value) {
            Customer::create($value);
        }
    }
}
