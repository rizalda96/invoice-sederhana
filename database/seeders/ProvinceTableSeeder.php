<?php

namespace Database\Seeders;

use App\Models\Province;
use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ["name" => "ACEH", "created_at" => now(), "updated_at" => now()],
            ["name" => "SUMATERA UTARA", "created_at" => now(), "updated_at" => now()],
            ["name" => "SUMATERA BARAT", "created_at" => now(), "updated_at" => now()],
            ["name" => "RIAU", "created_at" => now(), "updated_at" => now()],
            ["name" => "JAMBI", "created_at" => now(), "updated_at" => now()],
            ["name" => "SUMATERA SELATAN", "created_at" => now(), "updated_at" => now()],
            ["name" => "BENGKULU", "created_at" => now(), "updated_at" => now()],
            ["name" => "LAMPUNG", "created_at" => now(), "updated_at" => now()],
            ["name" => "KEPULAUAN BANGKA BELITUNG", "created_at" => now(), "updated_at" => now()],
            ["name" => "KEPULAUAN RIAU", "created_at" => now(), "updated_at" => now()],
            ["name" => "DKI JAKARTA", "created_at" => now(), "updated_at" => now()],
            ["name" => "JAWA BARAT", "created_at" => now(), "updated_at" => now()],
            ["name" => "JAWA TENGAH", "created_at" => now(), "updated_at" => now()],
            ["name" => "DI YOGYAKARTA", "created_at" => now(), "updated_at" => now()],
            ["name" => "JAWA TIMUR", "created_at" => now(), "updated_at" => now()],
            ["name" => "BANTEN", "created_at" => now(), "updated_at" => now()],
            ["name" => "BALI", "created_at" => now(), "updated_at" => now()],
            ["name" => "NUSA TENGGARA BARAT", "created_at" => now(), "updated_at" => now()],
            ["name" => "NUSA TENGGARA TIMUR", "created_at" => now(), "updated_at" => now()],
            ["name" => "KALIMANTAN BARAT", "created_at" => now(), "updated_at" => now()],
            ["name" => "KALIMANTAN TENGAH", "created_at" => now(), "updated_at" => now()],
            ["name" => "KALIMANTAN SELATAN", "created_at" => now(), "updated_at" => now()],
            ["name" => "KALIMANTAN TIMUR", "created_at" => now(), "updated_at" => now()],
            ["name" => "KALIMANTAN UTARA", "created_at" => now(), "updated_at" => now()],
            ["name" => "SULAWESI UTARA", "created_at" => now(), "updated_at" => now()],
            ["name" => "SULAWESI TENGAH", "created_at" => now(), "updated_at" => now()],
            ["name" => "SULAWESI SELATAN", "created_at" => now(), "updated_at" => now()],
            ["name" => "SULAWESI TENGGARA", "created_at" => now(), "updated_at" => now()],
            ["name" => "GORONTALO", "created_at" => now(), "updated_at" => now()],
            ["name" => "SULAWESI BARAT", "created_at" => now(), "updated_at" => now()],
            ["name" => "MALUKU", "created_at" => now(), "updated_at" => now()],
            ["name" => "MALUKU UTARA", "created_at" => now(), "updated_at" => now()],
            ["name" => "PAPUA BARAT", "created_at" => now(), "updated_at" => now()],
            ["name" => "PAPUA", "created_at" => now(), "updated_at" => now()]
        ];

        foreach ($data as $value) {
            Province::create($value);
        }
    }
}
