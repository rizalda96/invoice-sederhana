<?php

use App\Http\Controllers\InvoiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts/app');
// });

// Route::get('/', [InvoiceController::class, 'search'])->name('');
Route::get('/', [InvoiceController::class, 'index'])->name('index');
Route::get('/detail/{slug_path}', [InvoiceController::class, 'detail'])->name('detail-invoice');
