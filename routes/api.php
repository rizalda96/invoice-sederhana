<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['as' => 'invoice.'], function () {
    Route::get('invoice/search', [InvoiceController::class, 'search'])->name('search');
    Route::delete('invoice/remove', [InvoiceController::class, 'remove'])->name('remove');
    Route::post('invoice/store', [InvoiceController::class, 'store'])->name('store');
    Route::get('invoice/show', [InvoiceController::class, 'show'])->name('show');
    // Route::post('invoice/store/detail/:slug_path', [InvoiceController::class, 'detail'])->name('detail');
});

Route::group(['as' => 'customer.'], function () {
    Route::get('customer/search', [CustomerController::class, 'search'])->name('search');
});

Route::group(['as' => 'product.'], function () {
    Route::get('product/search', [ProductController::class, 'search'])->name('search');
    Route::get('product/detail', [ProductController::class, 'detail'])->name('detail');
});
